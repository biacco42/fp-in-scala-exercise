// package fpinscala.datastructures
sealed trait List[+A]
case object Nil extends List[Nothing]
case class Cons[+A](head: A, tail: List[A]) extends List[A]
object List {
  def sum(ints: List[Int]): Int = ints match {
    case Nil => 0
    case Cons(x, xs) => x + sum(xs)
  }

  def product(ds: List[Double]): Double = ds match {
    case Nil => 1.0
    case Cons(0.0, _) => 0.0
    case Cons(x, xs) => x * product(xs)
  }

  def apply[A](as: A*): List[A] = 
  if (as.isEmpty) Nil
  else Cons(as.head, apply(as.tail: _*))

  def tail[A](ds: List[A]): List[A] = ds match {
    case Nil => Nil
    case Cons(x, xs) => xs
  }

  def setHead[A](ds: List[A], head: A): List[A] = ds match {
    case Nil => Cons(head, Nil)
    // sys.error("setHead on empty list")
    case Cons(x, xs) => Cons(head, xs)
  }

  def drop[A](l: List[A], n: Int): List[A] = l match {
    case Nil => Nil
    case Cons(x, xs) if n > 0 => List.drop(xs, n - 1)
    case _ => l 
  }

  def dropWhile[A](l: List[A], f: A => Boolean): List[A] = l match {
    case Nil => Nil
    case Cons(x, xs) => if(f(x)) dropWhile[A](xs, f) else l
  }

  def init[A](l: List[A]): List[A] = l match {
    case Nil => Nil
    case Cons(x, Nil) => Nil
    case Cons(x, xs) => Cons(x, init[A](xs))
  }

  def foldRight[A, B](l: List[A], z: B)(f: (A, B) => B): B = l match {
    case Nil => z
    case Cons(x, xs) => f(x, foldRight[A, B](xs, z)(f))
  }

  def foldLeft[A, B](l: List[A], z: B)(f: (B, A) => B): B = l match {
    case Nil => z
    case Cons(x, xs) => foldLeft[A, B](xs, f(z, x))(f)
  }

  def sum2(l: List[Int]): Int = foldRight[Int, Int](l, 0)((x, y) => x + y)

  def product2(l: List[Double]): Double = foldRight[Double, Double](l, 1.0)(_ * _)

  def length[A](as: List[A]): Int = {
    def loop[A](bs: List[A], n: Int): Int = bs match {
      case Nil => n
      case Cons(x, xs) => loop[A](xs, n + 1)
    }
    loop[A](as, 0)
  }

  def length2[A](l: List[A]):Int = foldRight(l, 0)((_, acc) => acc + 1)

  def sumLeft(l: List[Int]): Int = foldLeft[Int, Int](l, 0)((x, y) => x + y)

  def productLeft(l: List[Double]): Double = foldLeft[Double, Double](l, 1.0)((x, y) => x * y)

  def lengthLeft[A](l: List[A]): Int = foldLeft[A, Int](l, 0)((acc, _) => acc + 1)
}

val test = List(1, 2, 3, 4, 5)
val test2 = List(1.0, 2.0 ,3.0, 4.0, 5.0)
println(List.tail[Int](test))
println(List.setHead[Int](test, 0))
println(List.drop[Int](test, 3))
println(List.dropWhile[Int](test, (v: Int) => v < 3))
println(List.init[Int](test))
println(List.sum2(test))
println(List.product2(test2))
println(List.length[Int](test))
println(List.length2[Int](test))
println(List.sumLeft(test))
println(List.productLeft(test2))
println(List.lengthLeft[Int](test))
